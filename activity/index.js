console.log("Hello, World!");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
function welcomeMessage(){
	let fullName = prompt("Enter your full name: ");
	let age = prompt("Enter your age: ");
	let location = prompt("Enter your location: ");

	console.log("Hello, " + fullName + "!");
	console.log("You are " + age + " years old");
	console.log("You live in " + location);
};

welcomeMessage();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
function top5BoyBands(){
	let one = "Backstreetboys";
	let two = "Boyz2Men";
	let three = "Westlife";
	let four = "Boyzone";
	let five = "N-sync";

	console.log("1. " + one);
	console.log("2. " + two);
	console.log("3. " + three);
	console.log("4. " + four);
	console.log("5. " + five);
};

top5BoyBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
function top5Movies(){
	let one = "Avengers: Endgame";
	let two = "Avengers: Infinity War";
	let three = "Spider-Man: No way Home";
	let four = "Avatar";
	let five = "Avatar: The way of water";

	console.log("1. " + one);
	console.log("Rotten Tomatoes Rating: 94%");
	console.log("2. " + two);
	console.log("Rotten Tomatoes Rating: 85%");
	console.log("3. " + three);
	console.log("Rotten Tomatoes Rating: 93%");
	console.log("4. " + four);
	console.log("Rotten Tomatoes Rating: 82%");
	console.log("5. " + five);
	console.log("Rotten Tomatoes Rating: 76%");
};

top5Movies()

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


/*
PROBLEM:
printUsers();
let printFriends() = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = alert("Enter your first friend's name:"); 
	let friend2 = prom("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friends); 
};


console.log(friend1);
console.log(friend2);
*/


// solution:
function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name: "); 
	let friend2 = prompt("Enter your second friend's name: "); 
	let friend3 = prompt("Enter your third friend's name: ");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};


printUsers();